# Pi-Hole Block Lists

## What is this for?
This is for [Pi-Hole](https://pi-hole.net/)

## Why Does this exist?
This exists as a version control for my master list. As well as a point to pull from should anyone wish to.

## Where do I put this?
Once you have Pi-Hole setup, go to `Settings` > `Blocklists` and paste in the following URL where it tells you to, then hit `Save & Update`.  
`https://gitlab.com/SimonRoth/pi-hole-block-lists/raw/master/masterblocks.txt`